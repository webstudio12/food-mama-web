import Vue from 'vue'
import VueRouter from 'vue-router'
import Axios from 'axios'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$http = Axios;
const accessToken = localStorage.getItem('access_token')

if (accessToken) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = accessToken
}

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: App },
    // { path: '/login', component: Login },
    // { path: '/about', component: About }
  ]
})

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
