import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'

Vue.use(Vuex);

//const debug = process.env.NODE_ENV !== 'production'
const debug = true;

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: JSON.parse(localStorage.getItem('user')) || {},
        cart: [],
        foods: null
    },

    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, payload) {
            state.status = 'success'
            state.token = payload.token
            state.user = payload.user
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            state.status = ''
            state.token = ''
            state.user = {}
        },
        foods(state, foods) {
            state.foods = foods
        },
        fetch_cart(state, cart) {
            state.cart = cart.data
        },
        add_to_cart(state, item) {
            state.cart.push(item);
        },
        update_cart_item(state, payload) {
            for (let i = 0; state.cart.length > i; i++) {
                if (state.cart[i].foodId === payload.item.foodId) {
                    if (payload.isCartInput) {
                        state.cart[i].qty = payload.qty;
                    } else {
                        state.cart[i].qty = state.cart[i].qty + 1;
                    }
                    state.cart[i].totalCost = (state.cart[i].unitPrice * state.cart[i].qty);
                }
            }
        },
    },

    actions: {
        login({ commit }, user) {
            user.strategy = 'local';
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({ url: 'http://foodmama.ng/api/v1/authentication', data: user, method: 'POST' })
                    .then(resp => {
                        const token = resp.data.accessToken
                        const user = resp.data.profile
                        let payload = {}
                        payload.token = token
                        payload.user = user
                        localStorage.setItem('token', token)
                        localStorage.setItem('user', JSON.stringify(user))
                        axios.defaults.headers.common['Authorization'] = token
                        commit('auth_success', payload)
                        resolve(resp)

                        this.dispatch("fetchCart")
                    })
                    .catch(err => {
                        commit('auth_error')
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },

        register({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({ url: 'http://foodmama.ng/api/v1/sign-up', data: user, method: 'POST' })
                    .then(resp => {
                        const token = resp.data.token
                        const user = resp.data.user
                        localStorage.setItem('token', token)
                        axios.defaults.headers.common['Authorization'] = token
                        commit('auth_success', token, user)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error', err)
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },

        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },

        fetchFood({ commit }) {
            return new Promise((resolve, reject) => {
                axios({ url: 'http://foodmama.ng/api/v1/food', method: 'GET' })
                    .then(resp => {
                        const foods = resp.data
                        //axios.defaults.headers.common['Authorization'] = token
                        commit('foods', foods)
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err)
                        reject(err)
                    })
            })
        },

        addToCart({ commit }, item) {
            return new Promise((resolve, reject) => {
                axios({ url: 'http://foodmama.ng/api/v1/cart', data: item, method: 'POST' })
                    .then(resp => {
                        const item = resp.data
                        commit('add_to_cart', item)
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },
        updateCart({ commit }, payload) {
            let cart_update = null;
            commit('update_cart_item', payload)
            for (let i = 0; this.state.cart.length > i; i++) {
                if (this.state.cart[i].foodId === payload.item.foodId) {
                    cart_update = this.state.cart[i];
                }
            }
            return new Promise((resolve, reject) => {
                axios({ url: `http://foodmama.ng/api/v1/cart/${cart_update._id}`, data: cart_update, method: 'PATCH' })
                    .then(resp => {
                        // const item = resp.data
                        this.dispatch("fetchCart")
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },
        removeFromCart({ commit }, item) {
            return new Promise((resolve, reject) => {
                axios({ url: `http://foodmama.ng/api/v1/cart?_id=${item._id}`, method: 'DELETE' })
                    .then(resp => {
                        //this.dispatch("fetchCart")
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },
        checkout({ commit }, item) {
            return new Promise((resolve, reject) => {
                axios({ url: 'http://foodmama.ng/api/v1/checkout', data: item, method: 'POST' })
                    .then(resp => {
                        this.dispatch("makePayment", resp)
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },
        makePayment({ commit }, order) {
            console.log(order);
            // createdAt: "2019-03-21T10:26:41.741Z"
            // deliveryAddress: "36, webminster str London"
            // orderId: "FM-1903217F38B"
            // orderStatus: "Created"
            // paymentStatus: "Not Paid"
            // profileId: "5c77bbeca89d84711c5ecfe4"
            // totalCost: 24000
            // updatedAt: "2019-03-21T10:26:41.772Z"
            let paymentData = {
                "paymentType": "credit",
                "paymentChannel": "card",
                "paymentRef": 479113607,
                "amount": order.totalCost,
                "profileId": order.profileId,
                "orderId": order.orderId
            }

            return new Promise((resolve, reject) => {
                axios({ url: 'http://foodmama.ng/api/v1/payment', data: paymentData, method: 'POST' })
                    .then(resp => {
                        //returns object of created order.
                        //i believe i should use this object to proceed to make payment
                        //this.dispatch("fetchCart")
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },

        fetchCart({ commit }) {
            return new Promise((resolve, reject) => {
                axios({ url: `http://foodmama.ng/api/v1/cart?profileId=${this.state.user._id}&cartStatus=Not CheckedOut`, method: 'GET' })
                    .then(resp => {
                        console.log(resp);
                        const cart = resp.data
                        commit('fetch_cart', cart)
                        resolve(resp)
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err)
                    })
            })
        },
    },

    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        getUser: state => state.user,
        cart: state => state.cart,
        getFoods: state => state.foods,
    },
    strict: debug
})