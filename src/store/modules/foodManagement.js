
import Api from '../Api'

//initial state
const state = {
    foods: null
}

export const getters = {

}

export const mutations = {

}

export const actions = {
    fetchFood({ commit }) {
        Api()
            .get('/food')
            .then(data => {
                console.log(data);
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
};